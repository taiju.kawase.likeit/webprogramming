<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>ユーザーユーザー情報詳細参照</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

</head>
<body>

	<div
		style="padding: 1px; margin-bottom: 1px; border: 1px solid #333333; width: 300px; margin-left: auto; margin-right: auto;">
		<div class="alert alert-dark" role="alert" style="text-align: right;">
			<small><small>${userInfo.name}さん</small></small>
			<a href="UserLogoutServlet" class="navbar-link logout-link">ログアウト</a>
		</div>

		<div style="text-align: center;">
			<h1 class="h3 mb-3 font-weight-normal">ユーザー情報詳細参照</h1>
			<br>
		</div>

		<small> ログインID</small> <small>${user.loginId}</small><br> <br>
		<small> ユーザー名</small> <small>${user.name}</small><br> <br> <small>
			生年月日</small> <small>${user.birthDate}</small><br> <br> <small>
			登録日時</small> <small>${user.createDate}</small><br> <br> <small>
			更新日時</small> <small>${user.updetaDate}</small><br> <br>


		<form class="form-signin" action="UserListServlet" method="get">
			<button type="submit" class="btn btn-link">
				<small><small>戻る</small></small>
			</button>
		</form>
	</div>
</body>
</html>


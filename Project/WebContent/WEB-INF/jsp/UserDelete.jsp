<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>ユーザー削除確認</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

</head>
<body>

	<div
		style="padding: 1px; margin-bottom: 1px; border: 1px solid #333333; width: 300px; margin-left: auto; margin-right: auto;">
		<div class="alert alert-dark" role="alert" style="text-align: right;">
			<small><small>ユーザー名さん</small></small>
			<a href="UserLogoutServlet" class="navbar-link logout-link">ログアウト</a>
		</div>


		<div style="text-align: center;">
			<h1 class="h3 mb-3 font-weight-normal">ユーザー削除確認</h1>
			<div style="text-align: left;">
				<small>ログインID：${user.id}</small><br> <small>を本当に削除してもよろしいでしょうか。</small>
				<br>
				<br>
				<br>
			</div>

			<form class="form-signin" action="UserListServlet" method="get">
				<small><input type="submit" style="width: 30%;"
					value="キャンセル"></small>
			</form>
			<form class="form-signin" action="UserDeleteServlet" method="post">
			<input type="hidden" name="id" value="${user.id}">
				<small><input type="submit" style="width: 30%" value="OK"></small>
			</form>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
		</div>
	</div>

</body>
</html>
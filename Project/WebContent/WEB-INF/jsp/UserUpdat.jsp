<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>ユーザー情報更新</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

</head>
<body>

	<div
		style="padding: 1px; margin-bottom: 1px; border: 1px solid #333333; width: 300px; margin-left: auto; margin-right: auto;">
		<div class="alert alert-dark" role="alert"
			style="text-align: right; align: center;">
			<small><small>${userInfo.name}さん</small></small>
			<a href="UserLogoutServlet" class="navbar-link logout-link">ログアウト</a>
		</div>



		<div style="text-align: center;">
			<h1 class="h3 mb-3 font-weight-normal">ユーザー情報更新</h1>
			<br>
		</div>

		<div class="container">
				<c:if test="${errMsg3 != null}">
					<div class="alert alert-danger" role="alert">${errMsg3}</div>
				</c:if>
			</div>

		<form class="form-signin" action="UserUpdatServlet" method="post">
			<input type="hidden" name="id" value="${user.id}"> <small>ログインID</small>
			${user.loginId}<br> <br> <small>パスワード</small> <input
				type="password" name="password"><br> <br> <small>パスワード(確認）</small><input
				type="password" name="password2"><br> <br> <small>ユーザー名</small>
			<input type="text" name="name" value="${user.name}"><br>
			<br> <small>生年月日</small> <input type="text" name="birthDate"
				value="${user.birthDate}"><br> <br>
			<div style="text-align: center;">
				<input type="submit" value="登録"><br>
			</div>
		</form>

		<form class="form-signin" action="UserListServlet" method="get">
			<button type="submit" class="btn btn-link">
				<small><small>戻る</small></small>
			</button>
		</form>
	</div>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

</head>
<body>
	<div
		style="padding: 1px; margin-bottom: 1px; border: 1px solid #333333; width: 300px; margin-left: auto; margin-right: auto; text-align: center;">
		<br>
			<h1 class="h3 mb-3 font-weight-normal">ログイン画面</h1> <br>

			<div class="container">
				<c:if test="${errMsg != null}">
					<div class="alert alert-danger" role="alert">${errMsg}</div>
				</c:if>




				<form class="form-signin" action="LoginServlet" method="post">
					<small>ログインID</small> <input type="text" name="loginId"><br>
					<br> <small>パスワード</small> <input type="password"
						name="password"><br> <br> <input type="submit"
						value="ログイン">
				</form>
				<br> <br> <br> <br>
			</div>
			</div>
</body>
</html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>ユーザー覧</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="table.css">
</head>
<body>

	<div
		style="padding: 1px; margin-bottom: 1px; border: 1px solid #333333; width: 600px; margin-left: auto; margin-right: auto;">
		<div class="alert alert-dark" role="alert" style="text-align: right;">
			<small><small>${userInfo.name}さん</small></small>
			<a href="UserLogoutServlet" class="navbar-link logout-link">ログアウト</a>
		</div>


		<div style="text-align: center">
			<h1 class="h3 mb-3 font-weight-normal">ユーザー覧</h1>
		</div>
		<div style="text-align: right;">
			<form class="form-signin" action="NewUserServlet" method="get">
				<button type="submit" class="btn btn-link">
					<small>新規登録</small>
				</button>
			</form>
		</div>

		<form class="form-signin" action="UserListServlet" method="post">
		<small>ログインID</small>
		<div style="text-align: center">
			<input type="text" style="width: 10em; height: 1em" name="loginId"><br>
		</div>
		<small>ユーザー名</small>
		<div style="text-align: center">
			<input type="text" style="width: 10em; height: 1em" name="name"><br>
		</div>
		<small>生年月日</small>
		<div style="text-align: center">
			<small><input type="text" style="width: 5.6em; height: 1.3em"
				name="birthDate" value="年/月/日"></small>〜<small><input type="text"
				style="width: 5.6em; height: 1.3em" name="birthDate2" value="年/月/日"></small>
		</div>
		<div style="text-align: right;">
			<input type="submit" style="width: 20%" value="検索"> <br>
			<br>
		</div>
		</form>

		<hr width="95%">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>ログインID</th>
					<th>ユーザ名</th>
					<th>生年月日</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="user" items="${userList}">
					<tr>
						<td>${user.loginId}</td>
						<td>${user.name}</td>
						<td>${user.birthDate}</td>
						<!-- TODO 未実装；ログインボタンの表示制御を行う -->
						<td><a class="btn btn-primary"
							href="UserInfoServlet?id=${user.id}"
							style="width: 20%; padding: 1px; font-size: 1px">詳細</a> <c:if test="${userInfo.name == '管理者'}"><a
							class="btn btn-success"
							style="width: 20%; padding: 1px; font-size: 1px"
							href="UserUpdatServlet?id=${user.id}">更新</a></c:if>  <c:if test="${userInfo.name == '管理者'}"><a
							class="btn btn-danger"
							style="width: 20%; padding: 1px; font-size: 1px"
							href="UserDeleteServlet?id=${user.id}">削除</a></c:if></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>

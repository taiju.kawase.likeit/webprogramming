package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDao {

	//ログインの処理
	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			String pass = encryption(password);

			//DBManagerのgetConnectionでデータベースに接続
			conn = DBManager.getConnection();

			//ログインIDとパスワードを選択するSQL
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			//SQLを実行し、結果を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, pass);
			ResultSet rs = pStmt.executeQuery();

			//失敗の処理 rs.next()が行われなかったらnullを返す
			if (!rs.next()) {
				return null;
			}
			//成功の処理
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// connがnullでなければ、データベース切断
			if (conn != null) {
				try {
					conn.close();
					//					return null;
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//全てのユーザー情報を取得
	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			//データベースに接続
			conn = DBManager.getConnection();

			//全取得SQL文
			String sql = "SELECT * FROM user WHERE NOT id = 1";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			//レコードの内容をインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public void newUser(String loginId, String password, String name, String birthDate) {
		Connection conn = null;
		try {
			String pass = encryption(password);

			//DBManagerのgetConnectionでデータベースに接続
			conn = DBManager.getConnection();

			//ログインIDとパスワードを選択するSQL
			String sql = "INSERT INTO user(login_id,password,name,birth_date,create_date,update_date) VALUES (?,?,?,?,NOW(),NOW())";

			//SQLを実行し、結果を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, pass);
			pStmt.setString(3, name);
			pStmt.setString(4, birthDate);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// connがnullでなければ、データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			}
		}
	}

	public boolean findUser(String loginId) {
		Connection conn = null;
		try {

			//DBManagerのgetConnectionでデータベースに接続
			conn = DBManager.getConnection();

			//ログインIDとパスワードを選択するSQL
			String sql = "SELECT * FROM user WHERE login_id = ?";

			//SQLを実行し、結果を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			//見つからなかった場合trueを返す
			if (!rs.next()) {
				return true;
			}
			return false;

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			// connがnullでなければ、データベース切断
			if (conn != null) {
				try {
					conn.close();
					//					return null;
				} catch (SQLException e) {
					e.printStackTrace();
					return false;
				}
			}
		}
	}

	public User UserInfo(String id) {
		Connection conn = null;
		try {

			//DBManagerのgetConnectionでデータベースに接続
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE id = ?";

			//SQLを実行し、結果を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			//失敗の処理 rs.next()が行われなかったらnullを返す
			if (!rs.next()) {
				return null;
			}
			//成功の処理
			int userid = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			User user = new User(userid, loginId, name, birthDate, password, createDate, updateDate);
			return user;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// connがnullでなければ、データベース切断
			if (conn != null) {
				try {
					conn.close();
					//					return null;
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public void UserUpdat(String id, String password, String name, String birthDate) {
		Connection conn = null;
		try {
			String pass = encryption(password);

			//DBManagerのgetConnectionでデータベースに接続
			conn = DBManager.getConnection();

			String sql = "UPDATE user SET password = ?,name = ?,birth_date = ?,update_date = NOW() WHERE id =? ";

			//SQLを実行し、結果を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, pass);
			pStmt.setString(2, name);
			pStmt.setString(3, birthDate);
			pStmt.setString(4, id);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// connがnullでなければ、データベース切断
			if (conn != null) {
				try {
					conn.close();
					//					return null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void UserDelete(String id) {
		Connection conn = null;
		try {

			//DBManagerのgetConnectionでデータベースに接続
			conn = DBManager.getConnection();

			String sql = "DELETE FROM user WHERE id = ? ";

			//SQLを実行し、結果を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// connがnullでなければ、データベース切断
			if (conn != null) {
				try {
					conn.close();
					//					return null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	public List<User> search(String loginIdP, String nameP,String birthDateP,String birthDate2) {

		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			//データベースに接続
			conn = DBManager.getConnection();

			//全取得SQL文
			String sql = "SELECT * FROM user WHERE NOT id = 1";

			 if(!loginIdP.equals("")) {
				 sql += " AND login_id = '" + loginIdP + "'";
			 }
			 if(!nameP.equals("")) {
				 sql += " AND name LIKE '" + "%" + nameP + "%" + "'";
			 }
			 if(!birthDateP.equals("年/月/日")) {
				 sql += " AND birth_date >= '" + birthDateP + "'";
			 }
			 if(!birthDate2.equals("年/月/日")) {
				 sql += " AND birth_date <= '" + birthDate2 + "'";
			 }

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			//レコードの内容をインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;

	}


	/*
	public List<User> search(String name) {
		List<User> userList = new ArrayList<User>();
		Connection conn = null;
		try {

			//DBManagerのgetConnectionでデータベースに接続
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE name LIKE ?";

			//SQLを実行し、結果を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, "%" + name + "%");

			ResultSet rs = pStmt.executeQuery();


			//成功の処理
			while (rs.next()) {
			int userid = rs.getInt("id");
			String loginid = rs.getString("login_id");
			String username = rs.getString("name");
			Date birthdate = rs.getDate("birth_date");
			String password = rs.getString("password");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			User user = new User(userid, loginid, username, birthdate, password, createDate, updateDate);

			userList.add(user);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// connがnullでなければ、データベース切断
			if (conn != null) {
				try {
					conn.close();
					//					return null;
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

*/
	static String encryption(String password) {

		//ハッシュを生成したい元の文字列
		String source = password;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);

		//標準出力
		return result;

	}
	public void UserUpdat2(String id, String name, String birthDate) {
		Connection conn = null;
		try {

			//DBManagerのgetConnectionでデータベースに接続
			conn = DBManager.getConnection();

			String sql = "UPDATE user SET name = ?,birth_date = ?,update_date = NOW() WHERE id =? ";

			//SQLを実行し、結果を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);
			pStmt.setString(2, birthDate);
			pStmt.setString(3, id);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// connがnullでなければ、データベース切断
			if (conn != null) {
				try {
					conn.close();
					//					return null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}


}


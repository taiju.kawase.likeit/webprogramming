
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdatServlet
 */
@WebServlet("/UserUpdatServlet")
public class UserUpdatServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserUpdatServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
	    User user = (User) session.getAttribute("userInfo");

	    if(user==null) {
	    	response.sendRedirect("LoginServlet");
	    }else {

		String id = request.getParameter("id");

		UserDao userDao = new UserDao();
		User info = userDao.UserInfo(id);

		request.setAttribute("user", info);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserUpdat.jsp");
		dispatcher.forward(request, response);
	    }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");

		UserDao userDao = new UserDao();

		if (!(password.equals(password2)) ||  name.equals("") || birthDate.equals("")) {
			request.setAttribute("errMsg3", "入力された文字は正しくありません");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserUpdat.jsp");
			dispatcher.forward(request, response);
			return;


			}else if(password.equals("")) {
				userDao.UserUpdat2(id,name, birthDate);
				response.sendRedirect("UserListServlet");
			}else {

			userDao.UserUpdat(id, password, name, birthDate);

			response.sendRedirect("UserListServlet");
			}

	}

}

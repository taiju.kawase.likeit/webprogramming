package model;

import java.io.Serializable;
import java.sql.Date;

public class User implements Serializable {
	private int id;
	private String loginId;
	private String name;
	private Date birthDate;
	private String password;
	private String createDate;
	private String updetaDate;

	//ログインセッションを保存するためのコネクション
	public User(String loginId, String name) {
		this.loginId = loginId;
		this.name = name;
	}

	//DBの全てのデータをセット
	public User(int id, String loginId, String name, Date birthDate, String password, String createDate,
			String updetaDate) {
		this.id = id;
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.password = password;
		this.createDate = createDate;
		this.updetaDate = updetaDate;

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdetaDate() {
		return updetaDate;
	}

	public void setUpdetaDate(String updetaDate) {
		this.updetaDate = updetaDate;
	}


}

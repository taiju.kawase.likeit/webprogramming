
import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserListServiet
 */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserListServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
	    User user = (User) session.getAttribute("userInfo");

	    if(user==null) {
	    	response.sendRedirect("LoginServlet");
	    }else {


		UserDao userDao = new UserDao();
		List<User> userList = userDao.findAll();

		//リクエストスコープにユーザー情報をセット
		request.setAttribute("userList", userList);

		//ユーザー覧を表示するフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/UserList.jsp");
		dispatcher.forward(request, response);
	    }


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String loginIdP = request.getParameter("loginId");
		String nameP = request.getParameter("name");
		String birthDateP = request.getParameter("birthDate");
		String birthDate2 = request.getParameter("birthDate2");

		UserDao userDao = new UserDao();
		List<User> search = userDao.search(loginIdP, nameP, birthDateP, birthDate2);

		request.setAttribute("userList", search);

		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/UserList.jsp");
		dispatcher.forward(request, response);
	}

}

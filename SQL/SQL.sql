CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;

CREATE TABLE user (
id SERIAL PRIMARY KEY AUTO_INCREMENT,
login_id varchar(255) UNIQUE Not Null,
name varchar(255) Not Null,
birth_date DATE Not Null,
password varchar(255) Not Null,
create_date DATETIME Not Null,
update_date DATETIME Not Null);

INSERT INTO user VALUES(1,'admin','管理者','1998-03-17','0317',NOW(),NOW());

